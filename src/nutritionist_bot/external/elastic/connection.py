import sys

from elasticsearch import AsyncElasticsearch
from loguru import logger

# local imports
from ...settings import settings
from .core import db


async def connect():
    logger.info('msg="Initializing es connection"')
    try:
        if not isinstance(db.es, AsyncElasticsearch):
            db.es = AsyncElasticsearch(
                # settings.elastic.url,
                timeout=30,
                max_retries=10,
                retry_on_timeout=True,
            )
        if not await db.es.ping():
            raise ConnectionError
    except Exception as exc:  # pylint: disable=broad-except
        logger.error("Failed connect to elastic")
        logger.error(str(exc))
        sys.exit(1)
    logger.success('msg="Successfully initialized es connection"')


async def disconnect():
    logger.info('msg="Closing es connection"')
    if db.es is not None:
        await db.es.close()
    else:
        logger.warning("No ES connection")
    logger.success('msg="Successfully Closed es connection"')
