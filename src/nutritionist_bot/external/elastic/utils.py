import sys

from loguru import logger
from datetime import datetime
from elasticsearch import AsyncElasticsearch, TransportError

from asyncio import sleep

from .core import db


def get_es() -> AsyncElasticsearch:
    if db.es is None:
        logger.error("Elastic is not available")
        sys.exit(1)
    return db.es


async def hard_query(
    query: dict,
    index: str,
    size: int,
):
    es = get_es()
    start = datetime.utcnow()
    while True:
        try:
            resp = await es.search(
                index=index,
                body={"size": size, "query": query},
            )
            results = []

            for doc in resp["hits"]["hits"]:
                results.append(doc["_source"])
            logger.debug(
                'msg="ElasticSearch request" '
                + f"es_time={(datetime.utcnow() - start).total_seconds()} "
                + f"took={resp.get('took', -1)} "
                + f"{index=}"
            )
            if results:
                results_dict = results[0]
                if isinstance(results_dict, dict):
                    return results_dict
            return {}
        except TransportError as te:
            logger.warning(
                f'msg="Elasticsearch TransportError, sleep 3 seconds" exception={te}'
            )
            await sleep(3)
        except Exception as exp:
            logger.error(
                'msg="Elasticsearch error" '
                + f"index={index} "
                + f"query={query} "
                + f'"exception="{exp}"'
            )
            raise exp
