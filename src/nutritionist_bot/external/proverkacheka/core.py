import json
from httpx import AsyncClient, HTTPStatusError, Response
from typing import Literal, Callable
from loguru import logger
from functools import wraps

from ...settings import settings


def catch_http_exception(func: Callable) -> Callable:
    @wraps(func)
    async def wrapper(*args, **kwargs) -> Response:
        response: Response = await func(*args, **kwargs)
        try:
            response.raise_for_status()
            return response
        except HTTPStatusError as e:
            logger.error(
                "metric_name='error message' "
                + f"reason={e.response.status_code} "
                + f"detile={e.response.json()}"
                + f"url='{e.request.url}'"
            )
            raise e

    return wrapper


class ProverkaCheka:
    def __init__(self, url: str, token: str) -> None:
        self._base_url = url
        self._token = token

    @catch_http_exception
    async def __request(
        self,
        method: Literal["get", "post"],
        path: str,
        json: dict | None = None,
    ):
        async with AsyncClient(
            headers={
                "Accept": "application/json",
                "charset": "UTF-8",
                "device-id": "",
                "device-os": "",
            },
        ) as client:
            if method == "get":
                response = await client.get(url=self._base_url + path)
            if method == "post":
                response = await client.post(url=self._base_url + path, json=json)
            else:
                raise ValueError(f"No such method: {method}")
            return response

    async def get_receipt_full_info(
        self,
        qrraw: str,
    ) -> dict:
        path = f"/check/get"
        response = await self.__request(
            "post",
            path,
            json={
                "qrraw": qrraw,
                "token": self._token,
            },
        )
        return json.loads(response.content)


proverkacheka = ProverkaCheka(
    settings.proverkacheka.url,
    settings.proverkacheka.token,
)
