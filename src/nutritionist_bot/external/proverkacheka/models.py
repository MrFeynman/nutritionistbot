from pydantic import BaseModel, Field, EmailStr


class SingUpModel(BaseModel):
    email: EmailStr = Field(
        examples=[
            "some@mail.com",
        ]
    )
    name: str = Field(
        examples=[
            "SomeName",
        ]
    )
    phone: str = Field(
        examples=[
            "+79991234567",
        ]
    )
