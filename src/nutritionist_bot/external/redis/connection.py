import sys

from loguru import logger
from redis import asyncio as aioredis

from ...settings import settings
from .core import redis_client


async def connect():
    logger.info("Initializing redis connection")
    if not settings.redis.address:
        return
    redis_client.redis = aioredis.from_url(
        settings.redis.address,
        encoding="utf-8",
        decode_responses=True,
    )
    if redis_client.redis is None:
        logger.error("Redis connection error")
        sys.exit(1)
    logger.success("Successfully initialized redis connection")
