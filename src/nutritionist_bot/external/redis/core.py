from redis import asyncio as aioredis
from ...settings import settings


class RedisClient:
    redis: aioredis.Redis | None = None


redis_client = RedisClient()
