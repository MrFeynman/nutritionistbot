import sys
import json
from loguru import logger
from redis import asyncio as aioredis

from .core import redis_client


def get_redis() -> aioredis.Redis:
    if redis_client.redis is None:
        logger.error("Redis connection error")
        sys.exit(1)
    return redis_client.redis
