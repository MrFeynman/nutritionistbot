import ssl
import sys

from asyncpg import create_pool
from asyncpg.pool import Pool
from loguru import logger

from ...settings import settings


class DataBase:
    pool: Pool | None = None
    results_pool: Pool | None = None


db = DataBase()


async def connect():
    logger.info("msg='Initializing PostgreSQL connection.'")
    if settings.postgres.certificate_flag:
        ctx = ssl.create_default_context(cafile="ca-certificate.crt")
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE
    else:
        ctx = None

    try:
        db.pool = await create_pool(  # type: ignore
            user=settings.postgres.user,
            password=settings.postgres.password,
            host=settings.postgres.host,
            port=settings.postgres.port,
            database=settings.postgres.database,
            min_size=0,
            max_size=15,
            max_inactive_connection_lifetime=60,
            ssl=ctx,
        )

    except Exception as exc:
        logger.error("msg='Failed connect to PostgreSQL.'")
        logger.error(str(exc))
        sys.exit(1)

    logger.success("msg='Successfully initialized PostgreSQL connection.'")


async def disconnect():
    logger.info("msg='Closing PostgreSQL connections.'")
    if db.pool is None:
        logger.warning("No PostgreSQL connection")
        return
    await db.pool.close()
    logger.success("Closed PostgreSQL connection")


def get_connection_pool() -> Pool:
    """
    https://magicstack.github.io/asyncpg/current/usage.html#connection-pools
    """
    if db.pool is None:
        logger.error("No PostgreSQL connection")
        sys.exit(1)
    return db.pool
