import re
import cv2
import numpy as np
from typing import BinaryIO

from ..external.proverkacheka import proverkacheka
from .models import ReceiptGetModel


def parse_qr_data(qr_data: str) -> ReceiptGetModel | None:
    results = [
        (res[1], res[2]) for res in re.findall(r"(\?|\&|^)([^=]+)\=([^&]+)", qr_data)
    ]
    if not results:
        return None
    results_dict = dict(results)
    return ReceiptGetModel(**results_dict)


async def get_receipt(qrraw: str):
    receipt = await proverkacheka.get_receipt_full_info(qrraw)
    return receipt.get("data", {}).get("json", {}).get("items", [])


async def analyse_photo(photo: BinaryIO):
    file_bytes = np.asarray(bytearray(photo.read()), dtype=np.uint8)
    img = cv2.imdecode(file_bytes, cv2.IMREAD_COLOR)
    detect = cv2.QRCodeDetector()
    value, _, _ = detect.detectAndDecode(img)
    if not value:
        return
    else:
        # receipt_info = parse_qr_data(value)
        # if receipt_info is None:
        #     return
        return await get_receipt(value)
