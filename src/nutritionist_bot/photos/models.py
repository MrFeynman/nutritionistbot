from pydantic import BaseModel, Field


class ReceiptGetModel(BaseModel):
    fn_num: str = Field(alias="fn")
    fd_num: str = Field(alias="i")
    fpd_num: str = Field(alias="fp")
