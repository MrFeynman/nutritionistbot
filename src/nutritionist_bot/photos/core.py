from aiogram import Router
from io import BytesIO
from typing import Any
import pandas as pd

from aiogram.types import Message

from .utils import analyse_photo

router = Router(name="by_photo")


async def send_long_message(message: Message, text: str):
    await message.answer(text=text[:4095])
    if len(text) > 4095:
        for x in range(4095, len(text), 4095):
            await message.answer(text=text[x : x + 4095])


def parce_receipt(receipt: list[dict[str, Any]]) -> str:
    df = pd.DataFrame(receipt)
    return df.to_string(columns=["name", "sum"], justify="right")


@router.message()
async def message_handler(message: Message):
    photos = message.photo
    if message.bot is None:
        raise Exception("No bot detected")
    if photos is not None:
        photo = await message.bot.download(
            file=photos[-1].file_id,
            destination=BytesIO(),
        )
        if photo is None:
            return
        result = await analyse_photo(photo)
        if result:
            parced_data = parce_receipt(result)
            await send_long_message(message, f"Got receipt\n{parced_data}")
        else:
            await message.answer(text="Broken qr code")
    elif message.document is not None:
        if message.document.mime_type is None:
            return
        if "image" in message.document.mime_type:
            photo = await message.bot.download(
                file=message.document.file_id,
                destination=BytesIO(),
            )
            if photo is None:
                return
            result = await analyse_photo(photo)
            if result:
                parced_data = parce_receipt(result)
                await send_long_message(message, f"Got receipt\n{parced_data}")
            else:
                await message.answer(text="Broken qr code")
    else:
        return
