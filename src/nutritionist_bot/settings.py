import re
from pydantic import BaseModel
from pydantic_settings import BaseSettings, SettingsConfigDict


class TGBotSettings(BaseModel):
    token: str
    admin_chat_id: int


class RedisSettings(BaseModel):
    address: str


class ProverkachekaSettings(BaseModel):
    url: str
    token: str


class PostgresSettings(BaseModel):
    host: str
    port: int
    database: str
    user: str
    password: str
    certificate_flag: bool = False


class Settings(BaseSettings):
    tg_bot: TGBotSettings
    redis: RedisSettings
    postgres: PostgresSettings
    proverkacheka: ProverkachekaSettings
    # elastic: ElasticSettings

    model_config = SettingsConfigDict(
        env_file=".env",
        env_file_encoding="utf-8",
        env_nested_delimiter=".",
    )


settings = Settings()  # type: ignore
