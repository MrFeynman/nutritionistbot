from aiogram import Bot, Dispatcher
from .external.postgres import (
    connect as connect_to_postgres,
    disconnect as disconnect_from_postgres,
)
from .photos.core import router as photo_router
from .external.redis import connect as connect_to_redis
from .settings import settings


async def on_startup(bot: Bot) -> None:
    await connect_to_postgres()
    await connect_to_redis()
    await bot.send_message(
        chat_id=settings.tg_bot.admin_chat_id,
        text="I've been started",
    )


async def on_shutdown(bot: Bot) -> None:
    await disconnect_from_postgres()
    await bot.send_message(
        chat_id=settings.tg_bot.admin_chat_id,
        text="I've been stopped",
    )


async def main():
    bot = Bot(token=settings.tg_bot.token)
    dp = Dispatcher()

    # Запускаем бота и пропускаем все накопленные входящие
    await bot.delete_webhook(drop_pending_updates=True)

    dp.startup.register(on_startup)
    dp.shutdown.register(on_shutdown)

    dp.include_router(photo_router)
    await dp.start_polling(bot)


def create_bot():
    return main()
