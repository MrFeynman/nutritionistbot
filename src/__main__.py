import asyncio

from nutritionist_bot.create_app import create_bot

if __name__ == "__main__":
    app = create_bot()
    asyncio.run(app)
